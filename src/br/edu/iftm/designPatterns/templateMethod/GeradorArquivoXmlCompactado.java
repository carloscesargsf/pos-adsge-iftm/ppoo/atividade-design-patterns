package br.edu.iftm.designPatterns.templateMethod;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class GeradorArquivoXmlCompactado extends GeradorDeArquivo {

    @Override
    protected String gerarConteudo(Map<String, Object> propriedades) {
        StringBuilder propFileBuilder = new StringBuilder();
        propFileBuilder.append("<properties>\n");
        for (String prop : propriedades.keySet()) {
            propFileBuilder.append("\t<" + prop + ">\n");
            propFileBuilder.append("\t\t" + propriedades.get(prop) + "\n");
            propFileBuilder.append("\t</" + prop + ">\n");
        }
        propFileBuilder.append("</properties>");

        return propFileBuilder.toString();
    }

    @Override
    protected byte[] processarConteudo(byte[] bytes) throws IOException {
        ByteArrayOutputStream byteout = new ByteArrayOutputStream();
        ZipOutputStream out = new ZipOutputStream(byteout);
        out.putNextEntry(new ZipEntry("internal"));
        out.write(bytes);
        out.closeEntry();
        out.close();

        return byteout.toByteArray();
    }

}
