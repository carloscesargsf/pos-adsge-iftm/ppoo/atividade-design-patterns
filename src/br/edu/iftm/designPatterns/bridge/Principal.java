package br.edu.iftm.designPatterns.bridge;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Principal {

    public static void main(String[] args) throws IOException {
        Map<String, Object> propriedades = new HashMap<>();
        propriedades.put("nome", "Carlos");
        propriedades.put("idade", 31);

        GeradorDeArquivo geradorArquivoPropriedadesCriptografado = new GeradorArquivoPropriedades(new ProcessaCriptografado());
        geradorArquivoPropriedadesCriptografado.gerarArquivo("C:\\Gerador\\Cripto.txt", propriedades);

        GeradorDeArquivo geradorArquivoXmlCompactado = new GeradorArquivoXml(new ProcessaCompactado());
        geradorArquivoXmlCompactado.gerarArquivo("C:\\Gerador\\XML.zip", propriedades);

        GeradorDeArquivo geradorArquivoPropriedadesDefault = new GeradorArquivoPropriedades(new ProcessaDefault());
        geradorArquivoPropriedadesDefault.gerarArquivo("C:\\Gerador\\default.txt", propriedades);
    }

}
