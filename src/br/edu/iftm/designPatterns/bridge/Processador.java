package br.edu.iftm.designPatterns.bridge;

import java.io.IOException;

public interface Processador {

    byte[] processarConteudo(byte[] bytes) throws IOException;

}
