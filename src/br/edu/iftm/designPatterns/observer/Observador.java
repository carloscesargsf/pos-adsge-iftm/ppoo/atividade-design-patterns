package br.edu.iftm.designPatterns.observer;

public interface Observador {

    public void novoArquivoGerado(String nomeArquivo, String conteudo);

}
