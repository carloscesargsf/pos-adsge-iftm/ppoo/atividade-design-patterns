package br.edu.iftm.designPatterns.observer;

public class LogService implements Observador {

    @Override
    public void novoArquivoGerado(String nomeArquivo, String conteudo) {
        System.out.println("Novo arquivo gerado com nome \"" + nomeArquivo + "\" e conteúdo \"" + conteudo + "\"");
    }

}
