package br.edu.iftm.designPatterns.observer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class LogDao {

    public void salvarLog(Log log) {
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:4002/dbArquivo", "root", "password");

            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO tb_logs(tipo, titulo, valor) values(" +
                            "?, ?, ?)");
            preparedStatement.setString(1, log.getTipo());
            preparedStatement.setString(2, log.getTitulo());
            preparedStatement.setString(3, log.getValor());

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

}
