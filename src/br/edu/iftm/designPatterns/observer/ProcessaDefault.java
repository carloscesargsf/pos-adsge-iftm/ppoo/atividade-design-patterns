package br.edu.iftm.designPatterns.observer;

import java.io.IOException;

public class ProcessaDefault implements Processador {

    public byte[] processarConteudo(byte[] bytes) throws IOException {
        return bytes;
    }

}
