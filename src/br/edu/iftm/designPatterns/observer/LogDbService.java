package br.edu.iftm.designPatterns.observer;

public class LogDbService implements Observador {
    @Override
    public void novoArquivoGerado(String nomeArquivo, String conteudo) {
        Log log = new Log();
        log.setTipo("Arquivo");
        log.setTitulo(nomeArquivo);
        log.setValor(conteudo);

        LogDao logDao = new LogDao();

        logDao.salvarLog(log);
    }
}
