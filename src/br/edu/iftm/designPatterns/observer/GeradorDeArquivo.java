package br.edu.iftm.designPatterns.observer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class GeradorDeArquivo {

    private Processador processador;

    private List<Observador> observadores;

    public GeradorDeArquivo(Processador processador) {
        this.processador = processador;
        observadores = new ArrayList<>();
    }

    public final void gerarArquivo(String nome, Map<String, Object> propriedades) throws IOException {
        String conteudo = gerarConteudo(propriedades);
        byte[] bytes = processador.processarConteudo(conteudo.getBytes());

        FileOutputStream fileout = new FileOutputStream(nome);
        fileout.write(bytes);
        fileout.close();

        notificar(nome, conteudo);
    }

    public void notificar(String nomeArquivo, String conteudo) {
        for (Observador observador : observadores) {
            observador.novoArquivoGerado(nomeArquivo, conteudo);
        }
    }

    public void adicionaObservador(Observador observador) {
        observadores.add(observador);
    }

    protected abstract String gerarConteudo(Map<String, Object> propriedades);

}
