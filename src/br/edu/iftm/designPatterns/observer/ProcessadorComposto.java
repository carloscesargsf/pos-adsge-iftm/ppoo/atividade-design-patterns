package br.edu.iftm.designPatterns.observer;

import java.io.IOException;
import java.util.List;

public class ProcessadorComposto implements Processador {

    private List<Processador> processadores;

    public ProcessadorComposto(List<Processador> processadores) {
        this.processadores = processadores;
    }

    @Override
    public byte[] processarConteudo(byte[] bytes) throws IOException {
        for (Processador processador : processadores) {
            bytes = processador.processarConteudo(bytes);
        }

        return bytes;
    }
}
