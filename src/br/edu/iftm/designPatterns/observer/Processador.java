package br.edu.iftm.designPatterns.observer;

import java.io.IOException;

public interface Processador {

    byte[] processarConteudo(byte[] bytes) throws IOException;

}
