package br.edu.iftm.designPatterns.composite;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

public abstract class GeradorDeArquivo {

    private Processador processador;

    public GeradorDeArquivo(Processador processador) {
        this.processador = processador;
    }

    public final void gerarArquivo(String nome, Map<String, Object> propriedades) throws IOException {
        String conteudo = gerarConteudo(propriedades);
        byte[] bytes = processador.processarConteudo(conteudo.getBytes());

        FileOutputStream fileout = new FileOutputStream(nome);
        fileout.write(bytes);
        fileout.close();
    }

    protected abstract String gerarConteudo(Map<String, Object> propriedades);

}
