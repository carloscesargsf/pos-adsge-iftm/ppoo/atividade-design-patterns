package br.edu.iftm.designPatterns.composite;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Principal {

    public static void main(String[] args) throws IOException {
        Map<String, Object> propriedades = new HashMap<>();
        propriedades.put("nome", "Carlos");
        propriedades.put("idade", 31);

        GeradorDeArquivo geradorArquivoPropriedadesCriptografado = new GeradorArquivoPropriedades(new ProcessaCriptografado());
        geradorArquivoPropriedadesCriptografado.gerarArquivo("C:\\Gerador\\Cripto.txt", propriedades);

        GeradorDeArquivo geradorArquivoXmlCompactado = new GeradorArquivoXml(new ProcessaCompactado());
        geradorArquivoXmlCompactado.gerarArquivo("C:\\Gerador\\XML.zip", propriedades);

        GeradorDeArquivo geradorArquivoPropriedadesDefault = new GeradorArquivoPropriedades(new ProcessaDefault());
        geradorArquivoPropriedadesDefault.gerarArquivo("C:\\Gerador\\default.txt", propriedades);

        List<Processador> processadoresCriptografadoECompactado = new ArrayList<>();
        processadoresCriptografadoECompactado.add(new ProcessaCriptografado());
        processadoresCriptografadoECompactado.add(new ProcessaCompactado());

        GeradorDeArquivo geradorArquivoPropriedadesCriptografadoECompactado = new GeradorArquivoPropriedades(new ProcessadorComposto(processadoresCriptografadoECompactado));
        geradorArquivoPropriedadesCriptografadoECompactado.gerarArquivo("C:\\Gerador\\Cripto.zip", propriedades);

        List<Processador> processadoresCompactadoECriptografado = new ArrayList<>();
        processadoresCompactadoECriptografado.add(new ProcessaCompactado());
        processadoresCompactadoECriptografado.add(new ProcessaCriptografado());

        GeradorDeArquivo geradorArquivoPropriedadesCompactadoECriptografado = new GeradorArquivoXml(new ProcessadorComposto(processadoresCompactadoECriptografado));
        geradorArquivoPropriedadesCompactadoECriptografado.gerarArquivo("C:\\Gerador\\xml_cripto.txt", propriedades);

        List<Processador> processadoresDefaultECompactado = new ArrayList<>();
        processadoresDefaultECompactado.add(new ProcessaDefault());
        processadoresDefaultECompactado.add(new ProcessaCompactado());

        GeradorDeArquivo geradorArquivoPropriedadesDefaultECompactado = new GeradorArquivoXml(new ProcessadorComposto(processadoresDefaultECompactado));
        geradorArquivoPropriedadesDefaultECompactado.gerarArquivo("C:\\Gerador\\default.zip", propriedades);
    }

}
