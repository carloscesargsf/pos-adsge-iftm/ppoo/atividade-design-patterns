package br.edu.iftm.designPatterns.composite;

import java.io.IOException;

public interface Processador {

    byte[] processarConteudo(byte[] bytes) throws IOException;

}
