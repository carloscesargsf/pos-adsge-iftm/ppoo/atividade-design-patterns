package br.edu.iftm.designPatterns.chainsOfResponsibility;

import java.io.IOException;

public abstract class Processador {

    private Processador proximoProcessador;

    public Processador() {
        proximoProcessador = null;
    }

    public Processador(Processador proximoProcessador) {
        this.proximoProcessador = proximoProcessador;
    }

    protected abstract byte[] processarConteudo(byte[] bytes) throws IOException;

    public byte[] processarCadeia(byte[] bytes) throws IOException {
        bytes = processarConteudo(bytes);

        return (proximoProcessador != null)
                ? proximoProcessador.processarCadeia(bytes)
                : bytes;
    }

}
