package br.edu.iftm.designPatterns.chainsOfResponsibility;

import java.io.IOException;

public class ProcessaDefault extends Processador {

    public ProcessaDefault() {
    }

    public ProcessaDefault(Processador proximoProcessador) {
        super(proximoProcessador);
    }

    @Override
    protected byte[] processarConteudo(byte[] bytes) throws IOException {
        return bytes;
    }

}
