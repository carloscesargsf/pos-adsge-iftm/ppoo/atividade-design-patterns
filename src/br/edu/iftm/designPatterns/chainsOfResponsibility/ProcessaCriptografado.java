package br.edu.iftm.designPatterns.chainsOfResponsibility;

public class ProcessaCriptografado extends Processador {

    public ProcessaCriptografado() {
    }

    public ProcessaCriptografado(Processador proximoProcessador) {
        super(proximoProcessador);
    }

    @Override
    protected byte[] processarConteudo(byte[] bytes) {
        byte[] newBytes = new byte[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            newBytes[i] = (byte) ((bytes[i] + 10) % Byte.MAX_VALUE);
        }

        return newBytes;
    }

}
