package br.edu.iftm.designPatterns.chainsOfResponsibility;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Principal {

    public static void main(String[] args) throws IOException {
        Map<String, Object> propriedades = new HashMap<>();
        propriedades.put("nome", "Carlos");
        propriedades.put("idade", 31);

        Processador processaCriptografado = new ProcessaCriptografado();
        GeradorDeArquivo geradorArquivoPropriedadesCriptografado = new GeradorArquivoPropriedades(processaCriptografado);
        geradorArquivoPropriedadesCriptografado.gerarArquivo("C:\\Gerador\\Cripto.txt", propriedades);

        Processador processaCompactado = new ProcessaCompactado();
        GeradorDeArquivo geradorArquivoXmlCompactado = new GeradorArquivoXml(processaCompactado);
        geradorArquivoXmlCompactado.gerarArquivo("C:\\Gerador\\XML.zip", propriedades);

        Processador processaDefault = new ProcessaDefault();
        GeradorDeArquivo geradorArquivoPropriedadesDefault = new GeradorArquivoPropriedades(processaDefault);
        geradorArquivoPropriedadesDefault.gerarArquivo("C:\\Gerador\\default.txt", propriedades);

        Processador processadoresCriptografadoECompactado = new ProcessaCriptografado(processaCompactado);
        GeradorDeArquivo geradorArquivoPropriedadesCriptografadoECompactado = new GeradorArquivoPropriedades(processadoresCriptografadoECompactado);
        geradorArquivoPropriedadesCriptografadoECompactado.gerarArquivo("C:\\Gerador\\Cripto.zip", propriedades);

        Processador processadoresCompactadoECriptografado = new ProcessaCompactado(processaCriptografado);
        GeradorDeArquivo geradorArquivoPropriedadesCompactadoECriptografado = new GeradorArquivoXml(processadoresCompactadoECriptografado);
        geradorArquivoPropriedadesCompactadoECriptografado.gerarArquivo("C:\\Gerador\\xml_cripto.txt", propriedades);

        Processador processadoresDefaultECompactado = new ProcessaDefault(processaCompactado);
        GeradorDeArquivo geradorArquivoPropriedadesDefaultECompactado = new GeradorArquivoXml(processadoresDefaultECompactado);
        geradorArquivoPropriedadesDefaultECompactado.gerarArquivo("C:\\Gerador\\default.zip", propriedades);
    }

}
