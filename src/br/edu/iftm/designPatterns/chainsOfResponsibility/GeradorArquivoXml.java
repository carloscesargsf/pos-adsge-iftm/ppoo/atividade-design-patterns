package br.edu.iftm.designPatterns.chainsOfResponsibility;

import java.util.Map;

public class GeradorArquivoXml extends GeradorDeArquivo {

    public GeradorArquivoXml(Processador processador) {
        super(processador);
    }

    @Override
    protected String gerarConteudo(Map<String, Object> propriedades) {
        StringBuilder propFileBuilder = new StringBuilder();
        propFileBuilder.append("<properties>\n");
        for (String prop : propriedades.keySet()) {
            propFileBuilder.append("\t<" + prop + ">\n");
            propFileBuilder.append("\t\t" + propriedades.get(prop) + "\n");
            propFileBuilder.append("\t</" + prop + ">\n");
        }
        propFileBuilder.append("</properties>");

        return propFileBuilder.toString();
    }

}
