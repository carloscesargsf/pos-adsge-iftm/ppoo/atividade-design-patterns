package br.edu.iftm.designPatterns.chainsOfResponsibility;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ProcessaCompactado extends Processador {

    public ProcessaCompactado() {
    }

    public ProcessaCompactado(Processador proximoProcessador) {
        super(proximoProcessador);
    }

    @Override
    protected byte[] processarConteudo(byte[] bytes) throws IOException {
        ByteArrayOutputStream byteout = new ByteArrayOutputStream();
        ZipOutputStream out = new ZipOutputStream(byteout);
        out.putNextEntry(new ZipEntry("internal"));
        out.write(bytes);
        out.closeEntry();
        out.close();

        return byteout.toByteArray();
    }

}
