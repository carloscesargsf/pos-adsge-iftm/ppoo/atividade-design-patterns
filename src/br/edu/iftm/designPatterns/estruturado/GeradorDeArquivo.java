package br.edu.iftm.designPatterns.estruturado;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class GeradorDeArquivo {

    public final void gerarArquivo(String nome, Map<String, Object> propriedades, String tipo) throws IOException {
        byte[] bytes = null;

        if ("PROPRIEDADES_CRIPTOGRAFADO".equals(tipo)) {
            StringBuilder propFileBuilder = new StringBuilder();
            for (String prop : propriedades.keySet()) {
                propFileBuilder.append(prop + "=" + propriedades.get(prop) + "\n");
            }

            bytes = propFileBuilder.toString().getBytes();

            byte[] newBytes = new byte[bytes.length];
            for (int i = 0; i < bytes.length; i++) {
                newBytes[i] = (byte) ((bytes[i] + 10) % Byte.MAX_VALUE);
            }
            bytes = newBytes;
        } else if ("XML_COMPACTADO".equals(tipo)) {
            StringBuilder propFileBuilder = new StringBuilder();
            propFileBuilder.append("<properties>\n");
            for (String prop : propriedades.keySet()) {
                propFileBuilder.append("\t<" + prop + ">\n");
                propFileBuilder.append("\t\t" + propriedades.get(prop) + "\n");
                propFileBuilder.append("\t</" + prop + ">\n");
            }
            propFileBuilder.append("</properties>");

            bytes = propFileBuilder.toString().getBytes();

            ByteArrayOutputStream byteout = new ByteArrayOutputStream();
            ZipOutputStream out = new ZipOutputStream(byteout);
            out.putNextEntry(new ZipEntry("internal"));
            out.write(bytes);
            out.closeEntry();
            out.close();
        } else {
            System.out.println("Desconheço esta opção.");
            return;
        }

        FileOutputStream fileout = new FileOutputStream(nome);
        fileout.write(bytes);
        fileout.close();
    }

}
