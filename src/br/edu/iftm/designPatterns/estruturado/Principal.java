package br.edu.iftm.designPatterns.estruturado;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Principal {

    public static void main(String[] args) throws IOException {
        Map<String, Object> propriedades = new HashMap<>();
        propriedades.put("nome", "Carlos");
        propriedades.put("idade", 31);

        GeradorDeArquivo geradorDeArquivo = new GeradorDeArquivo();
        geradorDeArquivo.gerarArquivo("C:\\Gerador\\Cripto.txt", propriedades, "PROPRIEDADES_CRIPTOGRAFADO");

        GeradorDeArquivo geradorDeArquivo1 = new GeradorDeArquivo();
        geradorDeArquivo1.gerarArquivo("C:\\Gerador\\XML.zip", propriedades, "XML_COMPACTADO");
    }

}
